import { TestBed, async } from '@angular/core/testing';

import { SpaceResolverService } from './space-resolver.service';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('SpaceResolverService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ HttpClientTestingModule, RouterTestingModule ]
    });
  }));

  it('should be created', () => {
    const service: SpaceResolverService = TestBed.get(SpaceResolverService);
    expect(service).toBeTruthy();
  });
});
