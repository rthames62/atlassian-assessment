import { Injectable } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { SpaceService } from 'src/app/libs/space/space.service';
import { Observable, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';
import { Space } from 'src/app/libs/types/space';

@Injectable({
  providedIn: 'root'
})
export class SpaceResolverService {

  constructor(private router: Router, private space: SpaceService) { }
  // Resolve the data from the API before the component loads
  resolve(route: ActivatedRoute): Observable<Space> {
    return this.space.getSpaces().pipe(
      mergeMap(spaces => {
        if (spaces) {
          // Unable to have the Osprey Mock Service generate the proper routes for grabbing an endpoint by id, so filtering here.
          spaces = spaces.filter(space => space.sys.id === route.params['id']);
          return spaces;
        } else {
          this.router.navigate(['/']);
          return EMPTY;
        }
      })
    )
  }
}
