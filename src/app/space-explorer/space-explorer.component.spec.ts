import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SpaceExplorerComponent } from './space-explorer.component';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { User } from '../libs/types/user';

describe('SpaceExplorerComponent', () => {
  let component: SpaceExplorerComponent;
  let fixture: ComponentFixture<SpaceExplorerComponent>;
  let mockUserService;
  let user: User;

  beforeEach(async(() => {
    mockUserService = jasmine.createSpyObj(['getUserById']);
    user = {
      fields: {name: 'Alana Atlassian', role: 'Author'},
      sys: {id: '4FLrUHftHW3v2BLi9fzfjU', type: 'User'}
    };
    TestBed.configureTestingModule({
      declarations: [ SpaceExplorerComponent ],
      imports: [ RouterTestingModule, HttpClientTestingModule ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SpaceExplorerComponent);
    component = fixture.componentInstance;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
