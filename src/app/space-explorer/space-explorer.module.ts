import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SpaceExplorerComponent } from './space-explorer.component';
import { RouterModule } from '@angular/router';
import { SpaceResolverService } from './resolver/space-resolver.service';
import { MatSortModule } from "@angular/material/sort";

@NgModule({
  declarations: [SpaceExplorerComponent],
  imports: [
    CommonModule,
    MatSortModule,
    RouterModule.forChild([
      {
        path: '',
        component: SpaceExplorerComponent,
        resolve: {
          space: SpaceResolverService
        }
      }
    ])
  ]
})
export class SpaceExplorerModule { }
