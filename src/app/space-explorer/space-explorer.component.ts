import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { SpaceService } from '../libs/space/space.service';
import { UserService } from '../libs/user/user.service';
import { Sort } from '@angular/material/sort';
import { Space, Entry, Asset } from '../libs/types/space';
import { User } from '../libs/types/user';

@Component({
  selector: 'aa-space-explorer',
  templateUrl: './space-explorer.component.html',
  styleUrls: ['./space-explorer.component.scss']
})
export class SpaceExplorerComponent implements OnInit {

  space: Space;
  entries: Entry[];
  assets: Asset[];
  createdByUser: User;
  showEntries: boolean;
  showAssets: boolean;

  constructor(
    private route: ActivatedRoute,
    private spaceService: SpaceService,
    private userService: UserService
  ) {

  }

  ngOnInit() {
    // Get data from resolved route
    this.route.data.subscribe(data => {
      this.space = data.space;
      this.userService.getUserById(this.space.sys.createdBy).subscribe(user => this.createdByUser = user);
    });
    // Subscribing to when there is a change in the route params
    this.route.params.subscribe(param => {
      // Fetching Entries
      this.spaceService.getSpaceEntriesById(param.id)
        .subscribe(entries => {
          this.entries = entries;

          // For each entry, I need to loop through and grab the correlating user by id.
          /*
          This is a dirty way of accomplishing this, in a normal circumstance I would have an endpoint that allowed me to include the user object on the original request rather than making multiple requests.
          */
          this.entries.forEach(entry => {
            this.userService.getUserById(entry.sys.createdBy)
              .subscribe(user => entry.sys.createdByUser = user);
            this.userService.getUserById(entry.sys.updatedBy)
              .subscribe(user => entry.sys.updatedByUser = user);
          });
        });
      // Fetching Assets
      this.spaceService.getSpaceAssetsById(param.id)
        .subscribe(assets => {
          this.assets = assets;

          this.assets.forEach(asset => {
            this.userService.getUserById(asset.sys.createdBy)
              .subscribe(user => asset.sys.createdByUser = user);
            this.userService.getUserById(asset.sys.updatedBy)
              .subscribe(user => asset.sys.updatedByUser = user);
          });
        });

        // Setting active table
        this.showEntries = true;
        this.showAssets = false;
    });
  }

  sortEntries(sort: Sort) {
    const isAsc: boolean = sort.direction === 'asc';

    if (!sort.active || sort.direction === '') {
      return;
    }

    this.entries.sort((a, b) => {
      switch (sort.active) {
        case 'title':
          return this.compare(a.fields.title, b.fields.title, isAsc);
        case 'summary':
          return this.compare(a.fields.summary, b.fields.summary, isAsc);
        case 'createdBy':
          return this.compare(a.sys.createdByUser.fields.name, b.sys.createdByUser.fields.name, isAsc);
        case 'updatedBy':
          return this.compare(a.sys.updatedByUser.fields.name, b.sys.updatedByUser.fields.name, isAsc);
        case 'lastUpdated':
          return this.compare(a.sys.updatedAt, b.sys.updatedAt, isAsc);
      }
    });
  }

  sortAssets(sort: Sort) {
    const isAsc: boolean = sort.direction === 'asc';

    if (!sort.active || sort.direction === '') {
      return;
    }

    this.assets.sort((a, b) => {
      switch (sort.active) {
        case 'title':
          return this.compare(a.fields.title, b.fields.title, isAsc);
        case 'contentType':
          return this.compare(a.fields.contentType, b.fields.contentType, isAsc);
        case 'fileName':
          return this.compare(a.fields.fileName, b.fields.fileName, isAsc);
        case 'createdBy':
            return this.compare(a.sys.createdByUser.fields.name, b.sys.createdByUser.fields.name, isAsc);
        case 'updatedBy':
          return this.compare(a.sys.updatedByUser.fields.name, b.sys.updatedByUser.fields.name, isAsc);
        case 'lastUpdated':
          return this.compare(a.sys.updatedAt, b.sys.updatedAt, isAsc);
      }
    });
  }

  private compare(a, b, isAsc: boolean) {
    return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
  }
}
