import { Component, OnInit } from '@angular/core';
import { SpaceService } from './libs/space/space.service';
import { Router } from '@angular/router';
import { ErrorHandlerService } from './libs/error-handler/error-handler.service';

@Component({
  selector: 'aa-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'atlassian-assessment';

  constructor(private space: SpaceService, private router: Router, private errorHandler: ErrorHandlerService) {

  }

  ngOnInit() {
    // Redirect to first item in spaces array
    this.space.getSpaces().subscribe(spaces => {
      this.router.navigate([`/spaceexplorer/${spaces[0].sys.id}`])
    });
  }

  triggerError() {
    this.errorHandler.error.next('I have triggered an error');
  }
}
