import { TestBed, async } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from './app.component';
import { NavigationModule } from './libs/navigation/navigation.module';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { ErrorHandlerModule } from './libs/error-handler/error-handler.module';

describe('AppComponent', () => {
  let fixture;
  let app;
  let mockSpaceService;
  let httpTestingController;

  beforeEach(async(() => {
    mockSpaceService = jasmine.createSpyObj(['getSpaces']);
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        NavigationModule,
        HttpClientTestingModule,
        ErrorHandlerModule
      ],
      declarations: [
        AppComponent
      ]
    }).compileComponents();

    fixture = TestBed.createComponent(AppComponent);
    httpTestingController = TestBed.get(HttpTestingController)
  }));

  it('should create the app', () => {
    app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });
});
