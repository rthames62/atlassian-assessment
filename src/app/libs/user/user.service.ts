import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { map, retry } from 'rxjs/operators';
import { User } from '../types/user';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) { }

  getUserById(id: string): Observable<User> {
    return this.http.get(`${environment.apiUrl}users/${id}`).pipe(
      map((user: User) => user),
      retry(3)
    );
  }
}
