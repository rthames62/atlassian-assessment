import { Component, OnInit } from '@angular/core';
import { SpaceService } from '../space/space.service';
import { Space } from '../types/space';

@Component({
  selector: 'aa-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss']
})
export class NavigationComponent implements OnInit {

  spaces: Space[] = [];

  constructor(private space: SpaceService) { }

  ngOnInit() {
    this.space.getSpaces().subscribe(spaces => this.spaces = spaces);
  }
}
