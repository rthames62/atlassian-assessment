import { Component, OnInit } from '@angular/core';
import { ErrorHandlerService } from './error-handler.service';

@Component({
  selector: 'aa-error-handler',
  templateUrl: './error-handler.component.html',
  styleUrls: ['./error-handler.component.scss']
})
export class ErrorHandlerComponent implements OnInit {

  error: string;

  constructor(private errorHandler: ErrorHandlerService) { }

  ngOnInit() {
    this.errorHandler.error.subscribe(err => this.error = err);
  }

  hideError() {
    this.error = '';
  }
}
