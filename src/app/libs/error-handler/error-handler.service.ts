import { Injectable } from '@angular/core';
import { throwError, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ErrorHandlerService {

  error: Subject<string> = new Subject();

  constructor() {

  }

  handleError = (error) => {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // client-side error
      errorMessage = `Error: ${error.error.message}`;
    } else {
      // server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    this.error.next(errorMessage);
    return throwError(errorMessage);
  }
}
