import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { map, retry, catchError } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { Space, SpacesResponse, Entry, EntriesResponse, Asset, AssetsResponse } from '../types/space';
import { ErrorHandlerService } from '../error-handler/error-handler.service';

@Injectable({
  providedIn: 'root'
})
export class SpaceService {

  constructor(private http: HttpClient, private errorHandler: ErrorHandlerService) {

  }

  getSpaces(): Observable<Space[]> {
    return this.http.get(`${environment.apiUrl}space`).pipe(
      map((res: SpacesResponse) => res.items),
      retry(3),
      catchError(this.errorHandler.handleError)
    );
  }

  getSpaceById(id: string): Observable<Space> {
    return this.http.get(`${environment.apiUrl}space/${id}`).pipe(
      map((space: any) => space),
      retry(3),
      catchError(this.errorHandler.handleError)
    );
  }

  getSpaceEntriesById(id: string): Observable<Entry[]> {
    return this.http.get(`${environment.apiUrl}space/${id}/entries`).pipe(
      map((res: EntriesResponse) => res.items),
      retry(3),
      catchError(this.errorHandler.handleError)
    );
  }

  getSpaceAssetsById(id: string): Observable<Asset[]> {
    return this.http.get(`${environment.apiUrl}space/${id}/assets`).pipe(
      map((res: AssetsResponse) => res.items),
      retry(3),
      catchError(this.errorHandler.handleError)
    );
  }
}
