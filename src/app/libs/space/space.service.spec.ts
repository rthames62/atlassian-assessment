import { TestBed, async } from '@angular/core/testing';

import { SpaceService } from './space.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('SpaceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ HttpClientTestingModule ]
    })
  }))

  it('should be created', () => {
    const service: SpaceService = TestBed.get(SpaceService);
    expect(service).toBeTruthy();
  });
});
