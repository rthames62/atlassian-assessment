import { User } from './user';

export interface GenericResponse {
  sys: { type: string };
  total: number;
  skip: number;
  limit: number;
}

export interface Sys {
  id: string;
  type: string;
  createdAt: string;
  createdBy: string;
  updatedAt: string;
  updatedBy: string;
  createdByUser?: User;
  updatedByUser?: User;
}
