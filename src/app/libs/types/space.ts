import { GenericResponse, Sys } from './generic';

export interface SpacesResponse extends GenericResponse {
  items: Space[];
}

export interface EntriesResponse extends GenericResponse {
  items: Entry[];
}

export interface AssetsResponse extends GenericResponse {
  items: Asset[];
}

export interface EntrySys extends Sys {
  version: number;
  space: string;
}

export interface AssetSys extends Sys {
  version: number;
  space: string;
}

export interface Space {
  fields: {
    title: string;
    description: string;
  };
  sys: Sys;
}

export interface Entry {
  fields: {
    title: string;
    summary: string;
    body: string;
  };
  sys: EntrySys;
}

export interface Asset {
  fields: {
    title: string;
    contentType: string;
    fileName: string;
    upload: string;
  };
  sys: AssetSys;
}
