import { GenericResponse } from './generic';

export interface UsersResponse extends GenericResponse {
  items: User[];
}

export interface User {
  fields: {
    name: string;
    role: string;
  };
  sys: {
    id: string;
    type: string;
  };
}

