const express = require('express');
const osprey = require('osprey');
const join = require('path').join;
const parser = require('raml-1-parser');
const mockService = require('osprey-mock-service');
const cors = require('cors');

const app = express();
const port = 3333;
const path = join(__dirname, 'data', 'space-api.raml');

app.use(cors());

parser.loadRAML(path, { rejectOnErrors: true }).then(mw => {
  const raml = mw.expand(true).toJSON({ serializeMetadata: false })
  app.use(osprey.server(raml));
  app.use(mockService(raml));

  app.listen(port, () => console.log(`API listening on port ${port}`));
}).catch(e => console.error(e));
