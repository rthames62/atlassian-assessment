# AtlassianAssessment

This project was created as an assessment for Atlassian.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`.
Run `npm run serve:api` to serve the api.

## Unit Tests

Run `ng test` to run the unit tests for the application
